import {expect} from 'chai';
import {
    fetchNearestDoctorHour, fetchNearestDoctorHours, fetchSpecialization,
    fetchSpecializations
} from "../../src/sagas/specializations";
import {doctorsRequest, servicesRequest, serviceCategoriesRequest} from "../../src/api/api";
import { takeEvery, put, call, select, all } from 'redux-saga/effects';
import {short_specializations_list} from "../fixtures/specializations";
import {selectWithCheck} from "../../src/helpers/selectWithCheck";
import {getSpecializationByCode} from "../../src/selectors/specializations";
import {
    doctor_with_specializations_1, doctor_with_specializations_1_4,
    doctor_with_specializations_8
} from "../fixtures/doctors";
import {schedule_2_57} from "../fixtures/schedule";
import {fetchVaccines} from "../../src/sagas/vaccines";

describe('specializations sagas' ,() => {

    it('fetchSpecializations', () => {
        const gen = fetchSpecializations();

        expect(gen.next().value).to.deep.equal(call(serviceCategoriesRequest));

        expect(gen.next(short_specializations_list).value)
            .to.deep.equal(put({type: 'FETCH_SPECIALIZATIONS_SUCCESS', value: short_specializations_list}));

        expect(gen.next().done).to.equal(true);
    });

    it('fetchSpecialization', () => {
        const gen = fetchSpecialization({specializationCode: 'xxx'});

        gen.next();

        expect(gen.next({id: 1}).value).to.deep.equal(put({type: 'SET_CURRENT_SPECIALIZATION_ID', value: 1}));

        expect(gen.next().value).to.deep.equal(all({
            doctors: call(doctorsRequest, {service_categories: 1}),
            services: call(servicesRequest, {categories: 1})
        }));

        expect(gen.next({doctors: [doctor_with_specializations_1_4], services: [{id: 112}]}).value)
            .to.deep.equal(put({type: 'FETCH_DOCTORS_SUCCESS', value: [doctor_with_specializations_1_4]}));

        expect(gen.next().value).to.deep.equal(
            put({type: 'SERVICES_SUCCESS', specialization: 1, value: [{id: 112}]}));
    });

    it('fetchSpecialization vakcinaciya', () => {
        const gen = fetchSpecialization({specializationCode: 'vakcinaciya'});

        expect(gen.next().value).to.deep.equal(call(fetchVaccines));
        expect(gen.next().done).to.equal(true);
    });

    it('fetchNearestDoctorHours', () => {

        const gen = fetchNearestDoctorHours({
            doctors: [
                doctor_with_specializations_1_4,
                doctor_with_specializations_1,
                doctor_with_specializations_8
            ],
            doctorsHours: schedule_2_57
        });

        gen.next();

        expect(gen.next("2017-11-08T09:09:43.514Z").value).to.deep.equal(
            all([fetchNearestDoctorHour(60, "2017-11-08T12:09:43")])
        );
    })
});