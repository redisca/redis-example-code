import {expect} from 'chai';
import {
    doctor_with_specializations_1, doctor_with_specializations_1_4,
    doctor_with_specializations_8
} from "../fixtures/doctors";
import {getOtherDoctors} from "../../src/selectors/doctor";

describe('doctor - getOtherDoctors' ,() => {

    it('getOtherDoctors', () => {

        const state = {
            doctor: {
                currentDoctor: doctor_with_specializations_1_4,
                otherDoctors: {
                    2: [60, 57]
                }
            },
            doctors: {
                doctors: [
                    doctor_with_specializations_1_4,
                    doctor_with_specializations_1,
                    doctor_with_specializations_8
                ]
            }
        };

        expect(getOtherDoctors(state)).to.deep.equal([doctor_with_specializations_8, doctor_with_specializations_1])
    });

    it('getOtherDoctors empty currentDoctor', () => {

        const state = {
            doctor: {
                otherDoctors: {
                    2: [60, 57]
                }
            },
            doctors: {
                doctors: [
                    doctor_with_specializations_1_4,
                    doctor_with_specializations_1,
                    doctor_with_specializations_8
                ]
            }
        };

        expect(getOtherDoctors(state)).to.deep.equal([])
    });

    it('getOtherDoctors empty otherDoctors', () => {

        const state = {
            doctor: {
                currentDoctor: doctor_with_specializations_1_4,
                otherDoctors: {
                }
            },
            doctors: {
                doctors: [
                    doctor_with_specializations_1_4,
                    doctor_with_specializations_1,
                    doctor_with_specializations_8
                ]
            }
        };

        expect(getOtherDoctors(state)).to.deep.equal([])
    });


});