

export const showMobileMenu = () => ({type: "SHOW_MOBILE_MENU"});

export const hideMobileMenu = () => ({type: "HIDE_MOBILE_MENU"});