import regeneratorRuntime from "regenerator-runtime"
import { put, call, all } from 'redux-saga/effects';
import {vaccinationsRequest, vaccinesRequest} from "../api/api";

export function* fetchVaccines() {
    try {
        const {vaccines, vaccinations} = yield all({
            vaccines: call(vaccinesRequest),
            vaccinations: call(vaccinationsRequest)
        });
        yield put({type: "FETCH_VACCINES_SUCCESS", value: vaccines});
        yield put({type: "FETCH_VACCINATIONS_SUCCESS", value: vaccinations})
    } catch (e) {
        yield put({type: "FETCH_VACCINES_ERROR"});
        console.log(e);
    }
}