import React from 'react';
import classNames from 'classnames';

import CSSTransitionGroup from 'react-addons-css-transition-group'
import {connect} from 'react-redux';
import * as actionCreators from '../../action_creators';

import {getCurrentCity, getCurrentCityName} from "../../selectors/geo";

import './citySelect.scss'

class CitySelect extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            isShown: false
        };
        this.clickCurrentCity = this.clickCurrentCity.bind(this);
        this.clickBackground = this.clickBackground.bind(this);
    }

    componentDidMount() {
        window.addEventListener("click", this.clickBackground, true);
    }

    componentWillUnmount() {
        window.removeEventListener("click", this.clickBackground);
    }

    clickCurrentCity() {
        this.setState({isShown: true});
    }

    clickBackground() {
        if (this.state.isShown) {
            this.setState({isShown: false});
        }
    }

    clickItem(cityId) {
        this.props.selectCity(cityId);
    }

    render() {
        return <span>
            <div className="citySelect text__color">
                <span onClick={this.clickCurrentCity}>{this.props.isAdverb ? this.props.currentCity.name_adverb : this.props.currentCity.name}</span>
                <div className="citySelect_arrow" />
                <CSSTransitionGroup transitionName="citySelect_transition"
                                    transitionEnterTimeout={300}
                                    transitionLeaveTimeout={150}>
                    {this.state.isShown ? <ul key={this.state.isShown} className="citySelect_list">
                        {this.props.cities.map((city)=>(<li key={city.id}
                                                            onClick={()=>this.clickItem(city.id)}
                                                            className={classNames("citySelect_item", {
                                                                "-current": this.props.currentCityId === city.id
                                                            })}>
                            {this.props.isAdverb ? city.name_adverb : city.name}
                        </li>))}
                    </ul> : null}
                </CSSTransitionGroup>

            </div>
        </span>
    }
}

function mapStateToProps(state) {
    return {
        cities: state.geo.cities,
        currentCityId: state.geo.currentCityId,
        currentCity: getCurrentCity(state)
    };
}


export default connect(mapStateToProps,
    actionCreators)(CitySelect);