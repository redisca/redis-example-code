import React from 'react';
import styled from 'styled-components';
import {FormLabel, StyledInput, StyledInputWithMask} from "../input/Field";
import {colors, media} from "../styleguide/Constants";
import {PSmall1} from "../styleguide/TextStyles";
import {Field} from "redux-form";
import {connect} from 'react-redux'
import {resendSmsConfirmationRequest} from "../../action_creators/appoinement";
import {AppointmentFormInputContainer} from "../appointment-popup/AppointmentFormInputContainer";

const SmsConfirmContainer = AppointmentFormInputContainer.extend`
    display: flex;
    justify-content: space-between;
    
    &:not(:first-child) {
        margin-top: 25px !important;
    }
    
    & ${StyledInput} {
        height: 50px;
    }
    
    @media (max-width: ${media.phone}) {
        flex-direction: column-reverse;
        
        &:not(:first-child) {
            margin-top: 15px !important;
        }
    }
    
    
`;

const SmsConfirmContainerMessage = styled.div`
    max-width: 240px;
    margin-top: 26px;
    margin-bottom: 38px;
    
    display: flex;
    flex-direction: column;
    align-items: flex-start;
    
    @media (max-width: ${media.phone}) {
        margin-top: 5px;
        margin-bottom: 20px;
    }
`;

const SmsConfirmInputWrapper = styled.div`
    max-width: 116px;
`;


const SmsConfirmResendSmsButton = PSmall1.extend`
    border: none;
    border-bottom: ${({isBlock}) => isBlock ? 'none' : `1px solid ${colors.greyishBrown}`};
    background: transparent;
    padding: 0;
    display: block;
    color: ${({isBlock}) => isBlock ? "#a5a5a5" : colors.black};
    
    margin-top: 15px;
    
    @media (max-width: ${media.phone}) {
        margin-top: 10px;
    }
`;

const SmsConfirmContainerCode = styled.div`
    & ${PSmall1} {
        display: block;
        margin-top: 6px;
    }
    
`;

const SmsConfirmFormInput = ({label, name, text, input, error, withMask, disabled, AppointmentFormInputContainer}) => {
    return <AppointmentFormInputContainer>
        <FormLabel disabled={disabled}
                   htmlFor={name}>
            {label}
        </FormLabel>
        <Input {...(withMask || {})}
               type="text" {...input}
               disabled={disabled}
               error={!disabled && error} />
    </AppointmentFormInputContainer>
};

export class SmsConfirm extends React.PureComponent {

    render() {
        const {isWaitSmsConfirm, isSmsCodeError, resendSmsConfirmationRequest, isBlockResendSms} = this.props;
        if (!isWaitSmsConfirm) {
            return null;
        }
        return <SmsConfirmContainer>
            <SmsConfirmContainerMessage>
                <PSmall1>
                    На указанный вами номер было отправлено SMS с кодом подтверждения.
                </PSmall1>
                <SmsConfirmResendSmsButton
                    isBlock={isBlockResendSms}
                    onClick={resendSmsConfirmationRequest}>
                    Отправить код повторно
                </SmsConfirmResendSmsButton>
            </SmsConfirmContainerMessage>

            <SmsConfirmContainerCode>
                <Field name="sms_code"
                       type="text"
                       component={SmsConfirmFormInput}
                       InputWrapper={SmsConfirmInputWrapper}
                       error={isSmsCodeError}
                       label="Код из СМС"/>
                {isSmsCodeError ? <PSmall1 error>Неверный код</PSmall1> : null}
            </SmsConfirmContainerCode>
        </SmsConfirmContainer>
    }

}

function mapStateToProps(state) {
    return {
        isWaitSmsConfirm: state.appointment.isWaitSmsConfirm,
        isSmsCodeError: state.appointment.isSmsCodeError,
        isBlockResendSms: state.appointment.isBlockResendSms
    }
}

export default connect(mapStateToProps, {resendSmsConfirmationRequest})(SmsConfirm);