import React from 'react';

import DoctorTableHeader from '../doctor-table-header/DoctorTableHeader'
import Doctor from '../doctor/Doctor'
import CitySelect from '../city-select/CitySelect'

import {connect} from 'react-redux';
import * as actionCreators from '../../action_creators';
import {getDoctorsWithSchedule, isShowLoadMoreDoctorsButton, getMoreDoctorsButtonCount} from "../../selectors/doctors";
import {countDoctors} from "../../helpers/currying";
import Breadcrumb from "../styleguide/Breadcrumb"
import { StickyContainer, Sticky } from 'react-sticky';
import _ from 'underscore'

import './doctorsPage.scss'
import {getCurrentCity, getCurrentCityUrlWrapper} from "../../selectors/geo";
import {Helmet} from "react-helmet";

class DoctorsPage extends React.PureComponent {
    constructor(props) {
        super(props);
    }

    render() {
        return <div className="doctorsPage">
            <Helmet>
                <title>Врачи сети клиник ... в {this.props.currentCity.name_adverb}</title>
                <meta name="description" content={`В ... ведут прием врачи всех специализаций:
                    гинекологи, урологи, эмбриологи, репродуктологи,
                    маммологи, флебологи, ЛОРы, травматологи, терапевты и другие.`} />
            </Helmet>
            <div className="intro">
                <div className="intro_main">
                    <Breadcrumb items={[
                        {to: '/', title: 'Главная', isWrappedCity: true},
                        {to: `/doctors`, title: 'Врачи', isWrappedCity: true},
                    ]} />
                    <h1 className="intro_title h1"><span>Врачи в </span>
                        <CitySelect isAdverb={true} />
                    </h1>
                </div>
            </div>
            <StickyContainer className="screen_wrapper">
                <Sticky>
                    {
                        ({style, isSticky}) => (
                                <div style={_.extend(style, {zIndex: 100}, isSticky  ? {} : {transform: 'none'})}>
                                    <DoctorTableHeader isSticky={isSticky} />
                                </div>
                        )
                    }
                </Sticky>
                {this.props.doctors.map((doctor)=>(<Doctor {...doctor} key={doctor.id} />))}
                {this.props.isShowMoreDoctors ? <button onClick={this.props.doctorsLoadMore} className="button button__border">
                    <span className="button_text">Еще {this.props.moreDoctorsCount} {countDoctors(this.props.moreDoctorsCount)}</span>
                </button> : null}
            </StickyContainer>


        </div>
    }
}

function mapStateToProps(state) {
    return {
        doctors: getDoctorsWithSchedule(state),
        isShowMoreDoctors: isShowLoadMoreDoctorsButton(state),
        moreDoctorsCount: getMoreDoctorsButtonCount(state),
        urlWrapper: getCurrentCityUrlWrapper(state),
        currentCity: getCurrentCity(state)
    };
}


export default connect(mapStateToProps,
    actionCreators)(DoctorsPage);
