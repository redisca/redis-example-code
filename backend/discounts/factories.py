from factory.django import DjangoModelFactory
import factory

from .models import Discount


class DiscountFactory(DjangoModelFactory):
    class Meta:
        model = Discount

    @factory.post_generation
    def service_categories(self, create, extracted, **kwargs):
        if not create:
            return
        if extracted:
            for spec in extracted:
                self.service_categories.add(spec)
