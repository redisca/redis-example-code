from django.contrib import admin
from .models import Discount


@admin.register(Discount)
class DiscountAdmin(admin.ModelAdmin):
    list_display = ('name', 'promo_code', 'date_start', 'date_finish')
    filter_horizontal = ('service_categories',)
