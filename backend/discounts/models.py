from django.db import models
from django.utils import timezone
from services.models import ServiceCategory


class DiscountManager(models.Manager):
    def actual(self):
        return self.get_queryset().exclude(
            models.Q(is_active=False) | models.Q(date_finish__lt=timezone.now().date()),
        )


class Discount(models.Model):
    objects = DiscountManager()
    name = models.CharField(max_length=255)
    short_description = models.TextField(blank=True)
    full_description = models.TextField(blank=True)
    service_categories = models.ManyToManyField(ServiceCategory, blank=True)
    promo_code = models.CharField(max_length=20, unique=True, blank=True, null=True)
    date_start = models.DateField(blank=True, null=True)
    date_finish = models.DateField(blank=True, null=True)
    is_public = models.BooleanField(default=True)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.name
