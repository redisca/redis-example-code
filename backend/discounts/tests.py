from rest_framework.test import APITestCase
from services.factories import ServiceCategoryFactory
from .factories import DiscountFactory


class DiscountsAPITest(APITestCase):
    list_url = '/discounts/'

    def test_check_promo_code(self):
        spec = ServiceCategoryFactory()
        discount = DiscountFactory(promo_code='1111', service_categories=[spec])

        response = self.client.post(self.list_url + 'check-promo-code/', format='json', data={
            'service_category': spec.id,
            'promo_code': '1111',
        })

        self.assertAlmostEqual(response.status_code, 200, msg=response.data)
        self.assertEqual(response.data['id'], discount.id)

    def test_check_promo_code_with_invalid_code(self):
        spec = ServiceCategoryFactory()
        DiscountFactory(promo_code='1111', service_categories=[spec])

        response = self.client.post(self.list_url + 'check-promo-code/', format='json', data={
            'service_category': spec.id,
            'promo_code': '2222',
        })

        self.assertAlmostEqual(response.status_code, 400)
        self.assertEqual(response.data['__all__'][0]['code'], 'invalid_promo_code')
