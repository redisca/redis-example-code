from rest_framework import viewsets
from rest_framework.decorators import list_route
from rest_framework.response import Response

from .serializers import DiscountSerializer, CheckPromoCodeSerializer
from .models import Discount


class DiscountViewSet(viewsets.ReadOnlyModelViewSet):
    def get_queryset(self):
        return Discount.objects.actual().filter(is_public=True)

    def get_serializer_class(self):
        if self.action == 'check_promo_code':
            return CheckPromoCodeSerializer
        else:
            return DiscountSerializer

    @list_route(['POST'])
    def check_promo_code(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        discount = Discount.objects.get(promo_code=serializer.validated_data['promo_code'])
        response_serializer = DiscountSerializer(discount)
        return Response(response_serializer.data)
