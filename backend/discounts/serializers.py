from rest_framework import serializers

from services.models import ServiceCategory
from utils.serializers import MarkdownField
from .models import Discount


class DiscountSerializer(serializers.ModelSerializer):
    short_description = MarkdownField()
    full_description = MarkdownField()

    class Meta:
        model = Discount
        fields = '__all__'


class CheckPromoCodeSerializer(serializers.Serializer):
    service_category = serializers.PrimaryKeyRelatedField(queryset=ServiceCategory.objects.all())
    promo_code = serializers.CharField()

    default_error_messages = {
        'invalid_promo_code': "Invalid promo code."
    }

    def validate(self, attrs):
        queryset = Discount.objects.actual().filter(
            service_categories=attrs['service_category'],
            promo_code=attrs['promo_code']
        )

        if not queryset.exists():
            self.fail('invalid_promo_code')

        return attrs
